
use anyhow::Result;
use gst::ClockTime;
use gst::DebugGraphDetails;
use gst::SeekFlags;
use gst::debug_bin_to_dot_file;
use gstreamer::prelude::*;
use gstreamer as gst;
use gstreamer::glib;

use gstreamer_video::VideoFormat;
use gst::element_error;
use gst::element_warning;

use std::env;
#[cfg(feature = "v1_10")]
use std::sync::{Arc, Mutex};
use std::time::Duration;

use derive_more::{Display, Error};

#[derive(Debug, Display, Error)]
#[display(fmt = "Missing element {}", _0)]
struct MissingElement(#[error(not(source))] &'static str);



pub fn player_main() -> Result<()> {

    gst::init()?;

    let args: Vec<_> = env::args().collect();
    let uri: &str = if args.len() == 2 {
        args[1].as_ref()
    } else {
        println!("Usage: decodebin file_path");
        std::process::exit(-1)
    };

    let pipeline = build_pipeline(uri)?;


    let bus = pipeline
        .bus()
        .expect("Pipeline without bus. Shouldn't happen!");

    //pipeline.sync_children_states()?;

    //    pipeline.set_clock(clock)
    // println!("=== PAUSE? ===");
    // std::thread::sleep(Duration::from_millis(1000));
    println!("---------------------- launch the pipeline ----------------------------------");
    pipeline.set_state(gst::State::Paused)?;
    std::thread::sleep(Duration::from_millis(1000));
    // pipeline.set_state(gst::State::Paused)?;
    // println!("=== PAUSE! ===");
    // std::thread::sleep(Duration::from_millis(3000));
    println!("---------------------- play the pipeline ----------------------------------");
    pipeline.set_state(gst::State::Playing)?;
    std::thread::sleep(Duration::from_millis(3000));
    debug_bin_to_dot_file(&pipeline, DebugGraphDetails::all(), "full_pipeline");
    // pipeline.set_state(gst::State::Paused)?;
    // std::thread::sleep(Duration::from_millis(1000));
    // pipeline.set_state(gst::State::Playing)?;
    // std::thread::sleep(Duration::from_millis(10000));


//    pipeline.set_clock(clock)
    // println!("=== PLAY? ===");
    // std::thread::sleep(Duration::from_millis(1000));
    // pipeline.set_state(gst::State::Playing)?;
    // println!("=== PLAY! ===");

    // This code iterates over all messages that are sent across our pipeline's bus.
    // In the callback ("pad-added" on the decodebin), we sent better error information
    // using a bus message. This is the position where we get those messages and log
    // the contained information.
    for msg in bus.iter_timed(gst::ClockTime::NONE) {
        use gst::MessageView;

        match msg.view() {
            MessageView::Eos(..) => {
                pipeline.seek_simple(SeekFlags::FLUSH, ClockTime::ZERO);
            },
            MessageView::Error(err) => {
                // pipeline.set_state(gst::State::Null)?;
                println!("MessageView::Error: {:?}", err);
                // #[cfg(feature = "v1_10")]
                // {
                //     match err.details() {
                //         // This bus-message of type error contained our custom error-details struct
                //         // that we sent in the pad-added callback above. So we unpack it and log
                //         // the detailed error information here. details contains a glib::SendValue.
                //         // The unpacked error is the converted to a Result::Err, stopping the
                //         // application's execution.
                //         Some(details) if details.name() == "error-details" => details
                //             .get::<&ErrorValue>("error")
                //             .unwrap()
                //             .clone()
                //             .0
                //             .lock()
                //             .unwrap()
                //             .take()
                //             .map(Result::Err)
                //             .expect("error-details message without actual error"),
                //         _ => Err(ErrorMessage {
                //             src: msg
                //                 .src()
                //                 .map(|s| String::from(s.path_string()))
                //                 .unwrap_or_else(|| String::from("None")),
                //             error: err.error().to_string(),
                //             debug: err.debug(),
                //             source: err.error(),
                //         }
                //         .into()),
                //     }?;
                // }
                // #[cfg(not(feature = "v1_10"))]
                // {
                //     return Err(ErrorMessage {
                //         src: msg
                //             .src()
                //             .map(|s| String::from(s.path_string()))
                //             .unwrap_or_else(|| String::from("None")),
                //         error: err.error().to_string(),
                //         debug: err.debug(),
                //         source: err.error(),
                //     }
                //     .into());
                // }
            }
            MessageView::StateChanged(s) => {
                println!(
                    "State changed from {:?}: {:?} -> {:?} ({:?})",
                    s.src().map(|s| s.path_string()),
                    s.old(),
                    s.current(),
                    s.pending()
                );
            }
            any => {
                // println!("Other Message #{:?}: {:?}", msg.seqnum(), any);
            },
        }
    }

    pipeline.set_state(gst::State::Null)?;




    Ok(())
}


const ENABLE_AUDIO: bool = true;

fn build_pipeline(url: &str)  -> Result<gst::Pipeline> {

    let pipeline = gst::Pipeline::new(None);

    //
    // Source
    //
    let filesource = gst::ElementFactory::make("filesrc", None).map_err(|_| MissingElement("filesrc"))?;
    // Tell the filesrc what file to load
    filesource.set_property("location", url);

    let decodebin = gst::ElementFactory::make("decodebin", None).map_err(|_| MissingElement("decodebin"))?;
    
    pipeline.add_many(&[&filesource, &decodebin])?;
    filesource.link(&decodebin)?;


    //
    // audio
    //
    println!("*** Build Audio Output Pipeline");
    let audioqueue = gst::ElementFactory::make("queue", None).map_err(|_| MissingElement("queue"))?;

    if ENABLE_AUDIO {
        audioqueue.set_property_from_str("max-size-buffers", "0");
        audioqueue.set_property_from_str("max-size-bytes", "0");
        audioqueue.set_property_from_str("max-size-time", "9000000000");

        let convert = gst::ElementFactory::make("audioconvert", None)
            .map_err(|_| MissingElement("audioconvert"))?;
        let resample = gst::ElementFactory::make("audioresample", None)
            .map_err(|_| MissingElement("audioresample"))?;
        // let aq2 = gst::ElementFactory::make("queue", None)
        //     .map_err(|_| MissingElement("queue"))?;

        let audiosink = gst::ElementFactory::make("decklinkaudiosink", None)
            .map_err(|_| MissingElement("decklinkaudiosink"))?;
        audiosink.set_property_from_str("sync", "true");
        audiosink.set_property("device-number",0);
        // audiosink.set_property_from_str("buffer-time", "900000");


        pipeline.add_many(&[&audioqueue, &convert, &resample, &audiosink])?;
        audioqueue.link(&convert)?;
        convert.link(&resample)?;
        resample.link(&audiosink)?;
        // aq2.link(&audiosink)?;

    }

    let aqpad = audioqueue.static_pad("sink").unwrap();

    //
    // video
    //
    let videoqueue = gst::ElementFactory::make("queue", None).map_err(|_| MissingElement("queue"))?;
    videoqueue.set_property_from_str("max-size-buffers", "0");
    videoqueue.set_property_from_str("max-size-bytes", "0");
    videoqueue.set_property_from_str("max-size-time", "9000000000");
    

    let convert = gst::ElementFactory::make("videoconvert", None)
        .map_err(|_| MissingElement("videoconvert"))?;

    println!("convert: {:?}", convert);

    let capsfilter =
        gst::ElementFactory::make("capsfilter", None).map_err(|_| MissingElement("capsfilter"))?;    

    println!("capsfilter: {:?}", capsfilter);

    let overlay = 
        gst::ElementFactory::make("gdkpixbufoverlay", None).map_err(|_| MissingElement("gdkpixbufoverlay"))?;
    overlay.set_property_from_str("location", "/home/mao/Videos/Pausenbildsteuerung/overlay.png");


    let aging = gst::ElementFactory::make("agingtv", None).map_err(|_| MissingElement("agingtv"))?; 

    let interlace = 
        gst::ElementFactory::make("interlace", None).map_err(|_| MissingElement("interlace"))?;    
    // let scale = gst::ElementFactory::make("videoscale", None)
    interlace.set_property_from_str("field-pattern", "2:2");
    println!("interlace: {:?}", interlace);



    // let vq2 = gst::ElementFactory::make("queue", None).map_err(|_| MissingElement("queue"))?;

    let videosink = gst::ElementFactory::make("decklinkvideosink", None)
        .map_err(|_| MissingElement("decklinkvideosink"))?;
    videosink.set_property("device-number",0);
    videosink.set_property_from_str("mode", "1080i50");
    videosink.set_property_from_str("sync", "true");
    // videosink.set_property_from_str("async", "true");
    // audiosink.set_property_from_str("buffer-time", "900000");


    println!("sink: {:?}", videosink);


    pipeline.add_many(&[&videoqueue, &convert, &capsfilter, &overlay, &aging, &interlace, &videosink])?;

    videoqueue.link(&convert)?;
    // aging.link(&convert)?;
    convert.link(&capsfilter)?;
    capsfilter.link(&overlay)?;
    overlay.link(&interlace)?;
    // aging.link(&interlace)?;
    interlace.link(&videosink)?;
    // vq2.link(&videosink)?;

    let vqpad = videoqueue.static_pad("sink").unwrap();


    // Plug in a capsfilter element that will force the videotestsrc and the overlay to work
    // with images of the size 800x800, and framerate of 15 fps, since my laptop struggles
    // rendering it at the default 30 fps
    let convert_caps = gst::Caps::builder("video/x-raw")
        // .field("width", 1920i32)
        // .field("height", 1080i32)
        .field("format", VideoFormat::Uyvy.to_str())
        .build();

    let resize_caps = gst::Caps::builder("video/x-raw")
        // .field("width", 1920i32)
        // .field("height", 1080i32)
        .field("format", VideoFormat::Uyvy.to_str())
        .build();

    println!("hi?");


    capsfilter.set_property("caps", &convert_caps);

    
    println!("hello?");



    decodebin.connect_pad_added(move |dcbin, src_pad| {

        println!("pad-added {:?} {:?}", dcbin, src_pad);
        // let name = pad.current_caps().get_name();

        let media_type = src_pad.current_caps().and_then(|caps| {
            caps.structure(0).map(|s| {
                let name = s.name();
                (name.starts_with("audio/"), name.starts_with("video/"))
            })
        });

        if let Some((aa,vv) ) = media_type {
            if aa && ENABLE_AUDIO {
                if let Err(e) = src_pad.link(&aqpad) {
                    println!("!!! cannot link aqpad {:?}", e);
                }
//                demux.link_pads(Some("demux.audio"), &audioqueue, Some("sink")).unwrap()
            }

            if vv {
                if let Err(e) = src_pad.link(&vqpad) {
                    println!("!!! cannot link aqpad {:?}", e);
                }
            }
        }



    });








    // let queue_pad = queue_back.static_pad("src").expect("interlace has no sourcepad");
    // let deckl_pad = sink.static_pad("sink").expect("interlace has no sourcepad");


    // queue_pad.link(&deckl_pad)?;


    //             for e in elements {
    //                 if let Err(xe) = e.sync_state_with_parent() {
    //                     println!("Sync Error for {:?} => {:?}", e, xe);
    //                     return Err(xe.into());
    //                 }
    //             }

    //             if let Err(xe) = sink.sync_state_with_parent() {
    //                 println!("Sync Error (sink) for {:?} => {:?}", sink, xe);
    //                 return Err(xe.into());
    //             };



    //             // Get the queue element's sink pad and link the decodebin's newly created
    //             // src pad for the video stream to it.
    //             let sink_pad = queue_front.static_pad("sink").expect("queue has no sinkpad");

    //             println!("now link to decodepad");
    //             std::thread::sleep(Duration::from_millis(500));
    //             src_pad.link(&sink_pad)?;
    //             println!("done");




    // let elements = &[&audioqueue, &convert, &resample, &sink];
    // pipeline.add_many(elements)?;
    // gst::Element::link_many(elements)?;

    // // !!ATTENTION!!:
    // // This is quite important and people forget it often. Without making sure that
    // // the new elements have the same state as the pipeline, things will fail later.
    // // They would still be in Null state and can't process data.
    // for e in elements {
    //     e.sync_state_with_parent()?;
    // }

    // // Get the queue element's sink pad and link the decodebin's newly created
    // // src pad for the audio stream to it.
    // let sink_pad = queue_front.static_pad("sink").expect("queue has no sinkpad");
    // src_pad.link(&sink_pad)?;






    Ok(pipeline)

}